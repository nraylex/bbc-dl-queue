"""
Listens to clipboard (xclip) and starts queing up the downloads if any BBC
programmes are added (copied).

use: python ./src/run.py
"""

import argparse
import logging
import os
import re
import json
import shutil
from datetime import datetime
from pathlib import Path
from queue import Queue
from subprocess import Popen, PIPE
from tempfile import mkdtemp
from threading import Thread
from time import sleep
from enum import Enum


XCLIP_BIN = "/usr/bin/xclip"
YOUTUBEDL_BIN = "/usr/bin/youtube-dl"

TEMPDIR = "/data/temp/youtube-dl-cache"        # TODO make it into a config var
PERSIST_FILE = "persist.json"

_format = "%(asctime)s: %(message)s"
logging.basicConfig(format=_format, level=logging.INFO, datefmt="%H:%M:%S")
logger = logging.getLogger()


BBC_ID_LEN = 8
BBC_URL_REs = (
    re.compile(r"www\.bbc\.co\.uk/sounds/play/[\w\d]{8}$"),
    re.compile(r"www\.bbc\.co\.uk/programmes/[\w\d]{8}$"),
)

YOUTUBEDL_AUDIO_FORMAT = "mp3"


def _get_bbc_url_id(url):
    return url[-BBC_ID_LEN:]


def _is_bbc_sounds_url(url):
    return any(map(lambda x: x.findall(url), BBC_URL_REs))


def _normalize_bbc_sounds_url(url):
    return url


def _is_in_library(url):
    """
    Checks if already in library tree (becasue of categories)
    """
    url_id = _get_bbc_url_id(url)
    return any(Path(os.getenv("BBC_RAD")).glob(f'**/*{url_id}.{YOUTUBEDL_AUDIO_FORMAT}'))


def _get_xclip_content():
    p = Popen([XCLIP_BIN, "-o"], stdout=PIPE, stderr=PIPE, universal_newlines=True)
    output, errors = p.communicate()
    return output


def _download_bbc_sounds(url, dry=False, category=""):

    # Find or create new output dir and path template
    url_id = _get_bbc_url_id(url)
    tempdir = Path(TEMPDIR)
    _tmp_dir = ""
    for _d in tempdir.iterdir():
        if _d.stem.endswith(url_id):
            _tmp_dir = _d.absolute()
            break
    if not _tmp_dir:
        _tmp_dir = mkdtemp(suffix=f"_youtube-dl_{url_id}", prefix=None, dir=TEMPDIR)
    tmpfile = os.path.join(_tmp_dir, "%(title)s-%(id)s.%(ext)s")

    # Gather metadata from url
    # creat
    _cmd = (
        "youtube-dl",
        "--output",
        tmpfile,
        "--ignore-errors",
        "--extract-audio",
        "--audio-format",
        YOUTUBEDL_AUDIO_FORMAT,
        url,
    )

    logger.info(f'\n # Downloading {url_id}    to {_tmp_dir} #:\n\n')
    if dry:
        logger.info(f" mock executing {_cmd}")
        for i in range(5):
            print(f'    .. {(100/5)*i}%')
            sleep(.5)
        os.rmdir(_tmp_dir)
        logger.info(f'\n # .. mock download finished {url_id} #:\n\n')
        return None

    # Run the actual download
    proc = Popen(_cmd,
        stderr=PIPE,
        stdin=PIPE,
        universal_newlines=True)

    _, stderr = proc.communicate()

    if stderr:
        raise RuntimeError(f"Failed tmpdir:{tmpfile}")

    logger.info(f'\n\n # .. download finished {url_id} #\n')

    # Move content of the temp dir to final denstinataion (with normalized names)
    tempdir = Path(_tmp_dir)
    for _f in tempdir.iterdir():
        # Replace the the odd charactes
        _fname = re.sub(" |,", "_", _f.stem)
        # restore to original id
        _fname = _fname[:-BBC_ID_LEN] + url_id
        # extension
        _fname += _f.suffix

        if category:
            _dest_path = os.path.join(os.getenv("BBC_RAD"), category)
            if not os.path.exists(_dest_path):
                os.mkdir(_dest_path)
        else:
            _dest_path = os.getenv("BBC_RAD")

        target = os.path.join(_dest_path, _fname)
        logger.info(f" Moving to local library: {target}")
        _f.replace(target)

    # Cleanup temp dir
    shutil.rmtree(_tmp_dir)

    return target


# from concurrent.futures import ThreadPoolExecutor
# ThreadPoolExecutor

# with ThreadPoolExecutor(max_workers=1) as executor:
#     future = executor.submit(download, url)
#     print(future.result())


class PersistState:
    UNKNOWN = 1
    COMPLETED = 2
    FAILED = 3
    CANCELLED = 4    # Todo, skip cancelled ones


class Persistence:
    """
    loads and stores job names, so we can resume if failed
    """
    def __init__(self):
        self._progs = {}
        self.dryrun = False
        self.load()

    def remove(self, url):
        raise NotImplementedError("Yet to add remove url from persistence list. For now just manually edit json")

    def add(self, url):
        if url not in self._progs:
            self._progs[url] = {"destin":"", "tmp":"", "state": PersistState.UNKNOWN, "time": datetime.now().timestamp(), "tries": 1, "name": ""}
            self.save()
        else:
            logger.warning(f"url {url} already in persistence cache")

    def completed(self, url, destin):
        self._progs[url]["state"] = PersistState.COMPLETED
        self._progs[url]["destin"] = destin
        self.save()

    def failed(self, url, tmpdir=""):
        self._progs[url]["state"] = PersistState.FAILED
        self._progs[url]["tries"] += 1
        if tmpdir:
            self._progs[url]["tmp"] = tmpdir
            tmpdir_ = Path(tmpdir).parent
            for i in tmpdir_.iterdir():
                self._progs[url]["name"] = i.name
                break
        self.save()

    def _get_by_id(self, id_):
        for i in self._progs:
            if i.endswith(id_):
                return i
        pass

    @property
    def _persist_path(self):
        local = os.path.split(os.path.split(__file__)[0])[0]
        return os.path.join(local, PERSIST_FILE)

    def save(self):
        if self.dryrun:
            logger.info("Pretend storing persistence data.")
            return
        with open(self._persist_path, "w") as pers:
            logger.info("Storing persistence data.")
            json.dump(self._progs, pers, indent=4)

    def load(self):
        if os.path.exists(self._persist_path):
            logger.info("Loading persistence data.")
            with open(self._persist_path, "r") as pers:
                self._progs = json.load(pers)

    @property
    def unfinished(self):
        for url in sorted(self._progs, key=lambda x: self._progs[x]["time"])[::-1]:
            if self._progs[url]["state"] != PersistState.COMPLETED:
                yield url


class BBCDownloader:

    def __init__(self, args=None):
        self._dry = False
        self._resume = False
        self._thread = None
        self._endless_mode = False
        self.__shutdown = False
        self._dlqueue = Queue()
        self._skip_retry = False
        self._last_url = ""
        self._completed_urls = []
        self._category = ""
        self._persist = Persistence()

        if args and args.dryrun:
            self._dry = True
            self._persist.dryrun = True
        if args and args.endless:
            self._endless_mode = True
        if args and args.category:
            self._category = args.category
        if args and args.resume:
            self._resume = True
        if args and args.skip_retry:
            self._skip_retry = True

    def _issue_downloads(self):
        while True:
            # Pull from queue, but block so we do one at the time
            url = self._dlqueue.get(block=True)
            url_id = _get_bbc_url_id(url)

            _copleted = False
            if url not in self._completed_urls:
                # Check if downloaded
                if not _is_in_library(url):
                    final_dest = None
                    try:
                        final_dest =_download_bbc_sounds(url, dry=self._dry, category=self._category)
                    except RuntimeError as er:
                        self._persist.failed(url, tmpdir=str(er).split("tmpdir:")[-1])
                        # Repeat task if retry enabled
                    if final_dest:
                        self._completed_urls.append(url)
                        # Mark complete persist
                        self._persist.completed(url, final_dest)
                        _copleted = True
                    elif not self._skip_retry:
                        logger.info(f"Adding {url_id} to retry list")
                        self._dlqueue.put(url)
                else:
                    logger.warning(f"Skipping: already in the library: {url_id}")
            else:
                logger.info(f"Skipping: already processed {url_id} ... you're wasting my time")

            logger.info(f"Number of links in queue: {self._dlqueue.qsize()}")

            self._dlqueue.task_done()

            # Kill off if not in endless mode
            if not self._endless_mode and self._dlqueue.qsize() == 0:
                logger.info("Queue finished. Exiting..")
                self.__shutdown = True

    def run(self):
        # turn-on the worker thread
        self._thread = Thread(target=self._issue_downloads, daemon=True)
        self._thread.start()
        if self._resume:
            for _url in self._persist.unfinished:
                logger.info(f"Continuing unfinished.. {_url}")
                self._dlqueue.put(_url)
                self._last_url = _url

        while True:
            _url = _get_xclip_content()
            if _url != self._last_url and _is_bbc_sounds_url(_url):
                self._persist.add(_url)
                self._dlqueue.put(_url)
                self._last_url = _url

            # Shutting down
            if self.__shutdown:
                return 0

            # no need to be hyperactive
            sleep(.1)

    def terminate(self):
        pass

    def list_persist_queue(self):
        _printed = False
        for url in sorted(self._persist._progs, key=lambda x: self._persist._progs[x]["time"])[::-1]:
            if self._persist._progs[url]["state"] is PersistState.COMPLETED:
                continue
            _date = datetime.fromtimestamp(self._persist._progs[url]['time']).date()
            _name = self._persist._progs[url]['name']
            print(f" - {_date} - {url} :  {_name}")
            _printed = True
        if not _printed:
            print("Download queue is empty.")

    def remove_persist_queue(self, url):
        if url in self._persist._progs:
            del self._persist._progs[url]
            self._persist.save()
            print(f"Removed url from queue: {url}")
        else:
            print(f"Url not in queue: {url}")



def main(args):
    if not os.path.exists(XCLIP_BIN):
        logger.error(f"System missing {XCLIP_BIN} . Make sure you have it installed")
        return 1
    if not os.path.exists(YOUTUBEDL_BIN):
        logger.error(f"System missing {YOUTUBEDL_BIN} . Make sure you have it installed")
        return 1
    if not os.getenv("BBC_RAD") or not os.path.exists(os.getenv("BBC_RAD")):
        logger.error("No BBC_RAD directory as environ.")
        return 1

    bbcdownloader = BBCDownloader(args)

    if args.queue_ls:
        bbcdownloader.list_persist_queue()
        return 0
    elif args.queue_rm:
        bbcdownloader.remove_persist_queue(args.queue_rm)
        return 0

    logger.info("Listening to clipboard for BBC urls... ")
    try:
        bbcdownloader.run()
    except KeyboardInterrupt:
        bbcdownloader.terminate()


def test(args):
    url = "https://www.bbc.co.uk/sounds/play/b006x3hl"
    url = "https://www.bbc.co.uk/sounds/play/b00zt60v"
    url = "https://www.bbc.co.uk/sounds/play/m00114py"
    assert _is_bbc_sounds_url(url)
    # assert _is_in_library(url)
    assert not _is_in_library("https://www.bbc.co.uk/sounds/play/m000vpm2")
    cout = _get_xclip_content()
    print(f"Clip contains: {cout}")
    assert cout
    # assert _is_bbc_sounds_url(cout), f"String in buffer is not bbc url: {cout}"
    print(f"category {args.category}")

    # Locally non available
    # url = "https://www.bbc.co.uk/programmes/p08gkvgf"
    if _is_bbc_sounds_url(cout):
        _download_bbc_sounds(cout, dry=args.dryrun)
    else:
        _download_bbc_sounds(url, dry=args.dryrun)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--dryrun', action='store_true', help='Runs dry (no download)')
    parser.add_argument('--endless', action='store_true', help="Will not terminate once the queue is empty.")
    parser.add_argument('--test', action='store_true', help="Run tests")
    parser.add_argument("--category", action="store", default=datetime.now().strftime("%y_%m"), type=str, help='Category by which the downloads will be stored.')
    parser.add_argument('--resume', action='store_true', help="Continue from previous state")
    parser.add_argument('--skip_retry', action='store_true', help="On failure won't retry to downloads failed.")
    parser.add_argument('--queue_ls', action='store_true', help="List all queued up urls (incomplete)")
    parser.add_argument('--queue_rm', action='store', type=str, help="Remove specific url from queue")
    args = parser.parse_args()

    if args.test:
        test(args)
    else:
        main(args)
