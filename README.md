# bbc-dl-queue

`youtube-dl` based bbc sounds download queue script. It listen's to the xclip buffer, and when it recognizes a valid bbc url it adds it to the queue.

Currently it runs on linux, but it shouldn't require much modification if someone wanted to use it on another os.

This project is a bit unpolished, but I figured I might share it in case others find it useful.

## In action

![Demo](snaps/bbc-demo.gif)

## Features

 - Organised date based download structure
 - Categoric downloads (groups specific urls into specific dir)
 - Persistence in case of failure
 - Download continuation (it doesn't start download from beginning)
 - Download retry in case of error

## Usage

```
python3 ./src/run.py [options]
```

## Option (--help)

```
  -h, --help           show this help message and exit
  --dryrun             Runs dry (no download)
  --endless            Will not terminate once the queue is empty.
  --test               Run tests
  --category CATEGORY  Category by which the downloads will be stored.
  --resume             Continue from previous state
  --skip_retry         On failure won't retry to downloads failed.
  --queue_ls           List all queued up urls (incomplete)
  --queue_rm QUEUE_RM  Remove specific url from queue
```

TODO:

[D] Mark cache with id (to resume download)
[D]  local json (so you can edit)
[D]    marks complete
[D]  --resume  (checks incomplete marked and corresponding files)
[D] rm error handling (usually happens)   **
    Dump untouched queue list on exit (for retry persistency) 
    bashrc alias
[D] Category persistence 
